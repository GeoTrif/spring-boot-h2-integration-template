package com.springbooth2intergrationtemplate.service;

import com.springbooth2intergrationtemplate.model.Person;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface PersonService {
    public List<Person> getAllPersons();

    public Person getPersonById(int id);

    public void saveOrUpdate(Person person);

    public void deletePerson(int id);
}
