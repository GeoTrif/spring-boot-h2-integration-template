package com.springbooth2intergrationtemplate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootH2IntergrationTemplateApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootH2IntergrationTemplateApplication.class, args);
	}

}
