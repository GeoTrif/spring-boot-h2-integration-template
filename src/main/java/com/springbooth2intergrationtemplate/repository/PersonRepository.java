package com.springbooth2intergrationtemplate.repository;

import com.springbooth2intergrationtemplate.model.Person;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends CrudRepository<Person, Integer> {
}
