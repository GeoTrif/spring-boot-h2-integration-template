package com.springbooth2intergrationtemplate.controller;

import com.springbooth2intergrationtemplate.model.Person;
import com.springbooth2intergrationtemplate.service.PersonService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PersonController {

    private PersonService personService;

    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping("/persons")
    public List<Person> getAllPersons() {
        return personService.getAllPersons();
    }

    @GetMapping("/persons/{id}")
    public Person getPerson(@PathVariable("id") int id) {
        return personService.getPersonById(id);
    }

    @DeleteMapping("/persons/{id}")
    public void deletePerson(@PathVariable("id") int id) {
        personService.deletePerson(id);
    }

    @PostMapping("/persons")
    public int savePerson(@RequestBody Person person) {
        personService.saveOrUpdate(person);

        return person.getId();
    }

}
