# **Spring Boot H2 Integration Template**

##### 1. Spring Boot overview

Spring Boot makes it easy to create stand-alone,production-grade Spring based Applications that you can "just run".

Keywords from definition:

- **stand-alone** – Spring Boot, by default supports a variety of embedded web servers which runs inside the application. This eliminates the need of deployment of any `war` files. It is important to understand that here the word "stand-alone" does **not** mean Java standard application.
- **production-grade** – Spring Boot applications are high quality and can be deployed to production as is.
- **just run** – Spring Boot applications can be simply run using the `main` function OR using the `java –jar` command. There is no need to deploy it to external web servers - though we are free to do so if needed.

Features:

- create stand-alone Spring applications

- Embed Tomcat,Jetty or Undertow directly(no need to deploy WAR files)

- provides 'opiniated' starter dependencies to simplify your build configuration

- automatically configure Spring and 3rd party libraries whenever possible

- provide production-ready features like metrics,health checks(actuator), and externalized configuration

- absolutely no code generation and no requirement for XML configuration
  
  

##### 2. H2 Database overview

H2 is an open source database and is written in Java.It is very fast and has very small size.It is primarily used as an in-memory database which means it stores the data in memory and will not persist data on disk.Althought if we need to persist data,it supports that as well.

H2 is ideal for quick PoC projects in which you need a simple database or you just want to test if you mapped your model entities correctly.



##### 3. Spring Boot Project build

a) Set up the project:

- Go to Spring Initializr: https://start.spring.io

- Customize your project by selecting Maven Project, Java Language,Spring Boot version,Project Metadata, Packaging and Java version.

- Set up the dependencies that you'll need to develop your application:I choose Spring Web,Spring Data JPA and H2 Database.

b) After you generated,downloaded and unzipped your generated project to a desired location,open it with IntelliJ or Eclipse.

c) In order  to enable our H2 database, we need to provide some database configurations.We will put these configuration in our application.properties file.

```properties
spring.datasource.url=jdbc:h2:mem:testdb
spring.datasource.driverClassName=org.h2.Driver
spring.datasource.username=sa
spring.datasource.password=
spring.h2.console.enabled=true

```

Here,we are specifying the datasource to use H2 

d) In order to demonstrate the H2 functionality,we are creating a simple REST service in which we'll interact with H2 using Spring Boot.

Our API will perform CRUD operations using different endpoints.

In this project,I structured my packages and classes using the MVC design pattern.

See the project for more details.



##### 4. API testing

a) Run the application class that has the main method in it to start the Spring Boot Application.After your app started,go into a browser and put this URL to navigate to: http://localhost:8080/h2-console

If the Spring Boot App was configured properly,you should see the console to connect to the H2 database.

Click Connect.

In here we can see our table named Person.We can make a select query to see the data that we currently have in our database.

```sql
select * from person;
```

H2 it's a in-memory database so it won't persist data by the current configurations we have in our project.Our data is loss when we stop the project from our IDE.

Let's set up some data in our database.Install Insomnia: https://insomnia.rest

We'll make a new POST request in order to save a person object in our database.

We can also get a person object from our database.We need to make a new GET request.
